<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;


//for localization
$locale = app('Illuminate\Http\Request')->server("HTTP_ACCEPT_LANGUAGE") ?? "en";
app('translator')->setLocale($locale);

//wallet api route
Route::group([
    "prefix" => "user",
    "middleware" => "auth",
    'namespace' => 'Version1'
], function () {
    Route::get('/test', function () {
        dd(__("messages.successfully_create"));
    });

    //user api routes
    Route::get('/', 'UserController@index');
    Route::post('/', 'UserController@store');
    Route::get('/{id}', 'UserController@show');
});

//wallet api route
Route::group([
    "prefix" => "wallet",
    "middleware" => "auth",
    'namespace' => 'Version1'
], function () {
    Route::get('/test', function () {
        dd(__("messages.successfully_create"));
    });

    //wallet api routes
    Route::get('/', 'WalletController@index');
    Route::post('/', 'WalletController@store');
    Route::get('/{id}', 'WalletController@show');
});

//account_type api routes
Route::group([
    "prefix" => "account_type",
    "middleware" => "auth",
    'namespace' => 'Version1'
], function () {

    Route::post('/', 'AccountTypeController@store');
    Route::get('/', 'AccountTypeController@index');
    Route::get('/{id}', 'AccountTypeController@show');
});


//account api routes
Route::group([
    "prefix" => "account",
    "middleware" => "auth",
    'namespace' => 'Version1'
], function () {

    Route::post('/', 'AccountController@store');
    Route::get('/', 'AccountController@index');
    Route::get('/{id}', 'AccountController@show');
    Route::post('/charge', 'AccountController@charge');
});

//order api routes
Route::group([
    "prefix" => "order",
    "middleware" => "auth",
    'namespace' => 'Version1'
], function () {

    Route::post('/', 'BasketController@store');
    Route::get('/', 'OrderController@index');
    Route::get('/{id}', 'OrderController@show');

    Route::post('/charge', 'OrderController@charge');
});


//product api routes
Route::group([
    "prefix" => "product",
    "middleware" => "auth",
    'namespace' => 'Version1'
], function () {

    Route::post('/', 'ProductController@store');
    Route::get('/', 'ProductController@index');
    Route::get('/{id}', 'ProductController@show');

});


/*------------------------------crud route---------------------------*/
/*Route::group(['prefix' => 'model'], function () {
    Route::get('/names', 'CrudController@models');
});

Route::group(['prefix' => 'crud', "namespace" => "Version1"], function () {
    Route::post('/{model}', 'CrudController@store');
    Route::get('/{model}', 'CrudController@list');
    Route::get('/{model}/{id}', 'CrudController@show');
    Route::delete('/{model}/{id}', 'CrudController@destroy');
    Route::put('/{model}/{id}', 'CrudController@update');
});*/

