<?php
return [
    "unique" => "field :attribute most be unique",
    "required" => "field :attribute can not be null",
    "max.string" => "field :attribute max length most be :max",
    "numeric" => "field :attribute most be numeric",
    "string" => "field :attribute most be string",

];
