<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_account', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("club_id")->index();
            $table->bigInteger("account_id")->index();
            $table->string("account_type")->nullable();
            $table->string("club_type")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_account');
    }
}
