<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger("from_account_id");
            $table->unsignedInteger("to_account_id");
            $table->string("type")->default("request");
            $table->bigInteger("amount");
            $table->unsignedInteger("basket_id")->nullable();
            $table->unsignedInteger("treasury_account_id")->nullable();
            $table->boolean("refund")->default(false);
            $table->boolean("cash_out")->default(false);
            $table->timestamp("paid_at")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_orders');
    }
}
