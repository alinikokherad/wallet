<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("merchant_id");
            $table->string("name");
            $table->string("title")->nullable();
            $table->text("description")->nullable();
            $table->boolean("promoted")->default(false);
            $table->string("status", 20)->default('active');
            $table->boolean("revoked")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_products');
    }
}
