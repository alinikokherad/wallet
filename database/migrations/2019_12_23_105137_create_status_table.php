<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("statusable_type");
            $table->string("statusable_id");
            $table->string("status");
            $table->string("title")->nullable();
            $table->string("subtitle")->nullable();
            $table->text("description")->nullable();
            $table->boolean("revoked")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_status');
    }
}
