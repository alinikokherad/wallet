<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasketCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basket_cost', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("basket_id")->index();
            $table->bigInteger("cost_id")->index();
            $table->integer("quantity")->nullable();
            $table->bigInteger("order_cost")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basket_cost');
    }
}
