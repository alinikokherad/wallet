<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_baskets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("from_id");
            $table->unsignedBigInteger("to_id");
            $table->string("description")->nullable();
            $table->bigInteger("type_id")->nullable();
            $table->bigInteger("author_id")->nullable();
            $table->bigInteger("status_id")->nullable();
            $table->boolean("revoked")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_baskets');
    }
}
