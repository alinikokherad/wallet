<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text("name")->nullable();
            $table->text("family")->nullable();
            $table->text("email")->nullable();
            $table->text("mobile");
            $table->text("degree")->nullable();
            $table->dateTime("mobile_verified_at")->nullable();
            $table->text("password")->nullable();
            $table->dateTime("birthday")->nullable();
            $table->enum("gender", ["male", "female"]);
            $table->enum("type", ["customer", "merchant", "super_admin", "club_admin", "waiter", "club", "treasury"]);
            $table->boolean("otp")->default(true);
            $table->enum("tier_id", ["bronze", "silver", "gold", "diamond"])->default("bronze");
            $table->json("settings")->nullable();
            $table->enum("status", ["active", "block", "disable", "revoked"])->default("active");
            $table->bigInteger("merchant_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_users');
    }
}
