<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_merchants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string("title")->nullable();
            $table->text("description")->nullable();
            $table->string("author")->nullable();
            $table->boolean("revoked")->default(false);
            $table->string("status")->default('active');
            $table->unsignedInteger("parent_id")->default(0);
//            $table->foreign("parent_id")->references("id")->on("merchants")->onUpdate("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_merchants');
    }
}
