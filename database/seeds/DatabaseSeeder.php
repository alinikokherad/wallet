<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(TranslateSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(WalletSeeder::class);
        $this->call(AccountTypeSeeder::class);

    }
}
