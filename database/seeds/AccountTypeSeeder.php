<?php

use App\Http\Controllers\Version1\AccountTypeController;
use Illuminate\Database\Seeder;

class AccountTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAccountType();
    }

    private function createAccountType()
    {
        $accountTypeController = new AccountTypeController();
        $accountTypeRequest = new \Illuminate\Http\Request();
        $items = ["customer" => "مشتری", "merchant" => "امتیاز", "admin" => "مدیر", "super_admin" => "مدیرکل", "operator" => "اپراتور", "club_admin" => "مدیر باشگاه"];
        $walletsInstance = \App\Wallet::all();
        foreach ($items as $type => $title) {
            foreach ($walletsInstance as $walletInstance) {
                $accountTypeData = [
                    "type" => $type,
                    "title" => $title,
                    "wallet_id" => $walletInstance->id
                ];
                $accountTypeRequest = $accountTypeRequest->replace($accountTypeData);
                $accountTypeController->store($accountTypeRequest);
            }
        }
    }
}
