<?php

use Illuminate\Database\Seeder;

class WalletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Wallet::truncate();
        \App\AccountType::truncate();
        \App\Account::truncate();
        \App\Credit::truncate();
        $this->walletCreate();
    }

    private function walletCreate()
    {
        $walletController = new \App\Http\Controllers\Version1\WalletController();
        $walletRequest = new \Illuminate\Http\Request();
        $items = ["rials" => "ریال", "point" => "امتیاز"];
        foreach ($items as $type => $title) {
            $walletData = [
                "type" => $type,
                "title" => $title,
            ];
            $walletRequest = $walletRequest->replace($walletData);
            $walletController->store($walletRequest);
        }

    }
}
