<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    $gender = $faker->randomElement(["male", "female"]);
    $mobile = "093757270" . $faker->randomNumber(2, true);
    $type = $faker->randomElement(["customer", "merchant", "super_admin", "club_admin", "waiter", "club", "treasury"]);
    return [
        'name' => $faker->name($gender),
        "family" => $faker->lastName($gender),
        'email' => $faker->email,
        "mobile" => $mobile,
        "degree" => $faker->randomElement(["high_school", "associate", "bachelor", "master", "phd"]),
        "password" => app("hash")->make(123),
        "gender" => $gender,
        "type" => $type,
        "tier_id" => $faker->randomElement(["bronze", "silver", "gold", "diamond"]),
        "merchant_id" => 1,
    ];
});
