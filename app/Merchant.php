<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Account
 * @package Mentasystem\Wallet\Entities
 */
class Merchant extends Model
{
    protected $table = "w_merchants";
    protected $fillable = [
        "name",
        "title",
        "description",
        "author",
        "revoked",
        "status",
        "parent_id",
    ];

    public function products()
    {
        return $this->hasMany(Product::class, "merchant_id");
    }

    public function users()
    {
        return $this->belongsToMany(User::class, "merchant_user", "merchant_id", "user_id");
    }

    public function parent()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function children()
    {
        return $this->hasMany(Merchant::class, "parent_id", "id");
    }

    public function clubs()
    {
        return $this->belongsToMany(Club::class, "club_merchant", "merchant_id", "club_id");
    }

    public function accounts()
    {
        return $this->hasMany(Account::class, 'client_id');
    }

    public function addresses()
    {
        return $this->morphOne(Address::class, "addressable");
    }

    public function categories()
    {
        return $this->hasMany(Category::class, "merchant_id");
    }

    public function baskets()
    {
        return $this->hasMany(Basket::class, "from_id");
    }
}
