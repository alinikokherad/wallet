<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Account
 * @package Mentasystem\Wallet\Entities
 */
class Cost extends Model
{
    protected $table = 'w_accounts';

    protected $fillable = [
        "product_id",
        "cost_type",
        "cost",
        "promote_id",
        "wallet_id",
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function baskets()
    {
        return $this->belongsToMany(Basket::class, "basket_cost", "cost_id", "basket_id")->withPivot(["quantity", "order_cost"]);
    }
}
