<?php

namespace App\Exceptions;

use App\Traits\Response;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    use Response;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * @param Exception $exception
     * @throws Exception
     */
    public function report(Exception $exception)
    {

        parent::report($exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        //handle exception
        if (env("APP_DEBUG")) {
            if ($exception instanceof ModelNotFoundException) {
                return $this->errorResponse("bad_query", $exception->getMessage(), 400);
            } elseif ($exception instanceof HttpResponseException) {
                $code = $exception->getstatusCode() == 0 ? 400 : $exception->getstatusCode();
                return $this->errorResponse("bad_url", $exception->getMessage(), $code);
            } elseif ($exception instanceof AuthorizationException) {
                $code = $exception->getstatusCode() == 0 ? 400 : $exception->getstatusCode();
                return $this->errorResponse("incorrect_authorization", $exception->getMessage(), $code);
            } elseif ($exception instanceof ValidationException) {
                $code = $exception->getstatusCode() == 0 ? 400 : $exception->getstatusCode();
                return $this->errorResponse("validation_error", $exception->getMessage(), $code);
            } elseif ($exception instanceof NotFoundHttpException) {
                $code = $exception->getstatusCode() == 0 ? 400 : $exception->getstatusCode();
                return $this->errorResponse("url_could_not_be_found", $exception->getMessage(), $code);
            } elseif ($exception instanceof InvalidArgumentException) {
                $code = $exception->getCode() == 0 ? 400 : $exception->getCode();
                return $this->errorResponse("invalid_argument", $exception->getMessage(), $code);
            }
        }
        return parent::render($request, $exception);
    }
}

