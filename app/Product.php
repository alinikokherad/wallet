<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Account
 * @package Mentasystem\Wallet\Entities
 */
class Product extends Model
{
    /**
     * @var string
     */
    protected $table = 'w_products';

    /**
     * @var array
     */
    protected $fillable = [
        "merchant_id",
        "name",
        "title",
        "description",
        "promoted",
        "status",
        "revoked",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function costs()
    {
        return $this->hasMany(Cost::class, "product_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function status()
    {
        return $this->morphMany(Status::class, "statusable", "statusable_type", "statusable_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, "category_product", "product_id", "cat_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributes()
    {
        return $this->belongsToMany(Attr::class, "w_values", "entity_id", "attr_id")->withPivot("value");
    }
}
