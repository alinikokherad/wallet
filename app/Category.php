<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Account
 * @package Mentasystem\Wallet\Entities
 */
class Category extends Model
{
    protected $table = 'w_categories';

    protected $fillable = [
        "name",
        "subtitle",
        "description",
        "merchant_id",
        "parent_id",
        "status",
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, "category_product", "cat_id", "product_id");
    }
}
