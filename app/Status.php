<?php

namespace App;

/**
 * Modules\Wallet\Entities
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @mixin \Eloquent
 */

use App\Http\Controllers\Version1\BasketController;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public $validate = [
        "title",
        "type",
        "revoked",
    ];
    protected $table = "w_status";

    protected $fillable = [
        "status",
        "title",
        "subtitle",
        "description",
        "revoked",
    ];


    public function statusable()
    {
        return $this->morphTo();
    }

}
