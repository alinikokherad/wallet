<?php

namespace App\Http\Controllers\Version1;

use App\Account;
use App\repo\AccountDB;
use App\repo\AccountTypeDB;
use App\repo\CreditDB;
use App\repo\StatusDB;
use App\repo\WalletDB;
use App\Traits\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class StatusController extends Controller
{
    use Response;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $statusDB = new StatusDB();
        $limit = app("request")->has("limit") ? app("request")->input("limit") : 10;
        $response = $statusDB->get($limit);
        return $this->successResponse("status_list", $response, 200);
    }

    /**
     * @param AccountDB $repository
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(AccountDB $repository, $id)
    {
        $account = $repository->getAccount($id);
        return $this->successResponse("account_show", $account, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $validate = $this->validation($request->all(), [
            "user_id" => "nullable|numeric",
            "account_type" => "required|string",
            "revoked" => "nullable",
        ]);
        if ($validate) {
            return $validate;
        }

        //initialize variable
        $accountDB = new AccountDB();
        $walletDB = new WalletDB();
        $accountTypeDB = new AccountTypeDB();
        $creditDB = new CreditDB();
        $user_id = $request->user_id;

        try {
            \DB::beginTransaction();

            //find account type
            $accountTypes = $accountTypeDB->getAccountTypeWithType($request->account_type);

            foreach ($accountTypes as $accountType) {
                //get treasury account
                $treasuryAccountInstance = $walletDB->getTreasuryAccountInstanceWithWalletId($accountType->wallet_id);

                //create account
                $accountData = [
                    "user_id" => $user_id,
                    "treasury_id" => $treasuryAccountInstance->id,
                    "account_type_id" => $accountType->id,
                ];
                $accountInstance = $accountDB->create($accountData);

                //check for successfully create account
                if (!$accountInstance) {
                    throw new \Exception(__("messages.account_create_fail"), \Illuminate\Http\Response::HTTP_BAD_REQUEST);
                }

                //create credit
                $creditData = [
                    "account_id" => $accountInstance->id,
                    "treasury_id" => $treasuryAccountInstance->id,
                    "amount" => 0,
                    "usable_at" => null,
                    "expired_at" => null,
                    "revoked" => false,
                ];
                $creditDB->create($creditData);
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            //failed response before create account
            return $this->errorResponse("400", $e->getMessage(), $e->getCode());
        }
        //success response after create account
        return $this->successResponse("account_create_successfully", $accountInstance, 200);
    }

    /**
     * @param Requset $request
     * @param AccountDB $repository
     * @param $id
     * @return mixed
     */
    public function update(Requset $request, AccountDB $repository, $id)
    {
        $data = $repository
            ->convertRequestToArray($request);
        $account = Account::where('id', $id)
            ->update($data);
        return $account;
    }

    /**
     * @param AccountDB $repository
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $accountDB = new AccountDB();
        $accountDB->deleteAccount($id);
        return \response()
            ->json([
                null
            ], 204);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function charge(Request $request)
    {
        $validate = $this->validation($request->all(), [
            "account_id" => "required",
            "user_id" => "required",
            "amount" => "required",
            "wallet_type" => "string|required",
        ]);
        if ($validate) {
            return $validate;
        }

        try {
            /*----------charge account credit-----------*/
            \DB::beginTransaction();
            $walletDB = new WalletDB();
            $accountDB = new AccountDB();

            //convert wallet to treasury id
            //get wallet id
            $walletInstance = $walletDB->get($request->wallet_type);

            //get user treasury account instance
            $treasuryAccountInstance = $walletDB->getTreasuryAccountInstanceWithWalletId($walletInstance->id);
//            dd($treasuryAccountInstance->toArray());

            //get account
            $bankAccount = $accountDB->find($request->account_id, $where = ["treasury_id", $treasuryAccountInstance->id]);

            //get user related account
            $userAccountInstance = $accountDB->findWhere([["user_id", "=", $request->user_id], ["treasury_id", "=", $treasuryAccountInstance->id]], true);

            //submit order
            $orderController = new OrderController();
            $orderController->store();


            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return response()
                ->json([
                    "message" => "some thing went wrong"
                ], 400);
        }

        return response()
            ->json([
                "message" => "account successfully charged"
            ], 200);
    }
}
