<?php

namespace App\Http\Controllers\Version1;

use App\Http\Controllers\Controller;
use App\repo\AccountDB;
use App\repo\CostDB;
use App\repo\MerchantDB;
use App\repo\ProductOrderDB;
use App\repo\UserDB;
use App\repo\WalletDB;
use App\User;
use Illuminate\Http\Request;

/**
 * Class ProductOrderController
 * @package Modules\Wallet\Http\Controllers
 */
class BasketController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $payment = $request->has('payment') ? (boolean)$request->payment : null;
        $mobile = $request->has('mobile') ? $request->mobile : null;
        $page = $request->has('page') ? $request->page : 1;
        $limit = $request->has('limit') ? $request->limit : 10;
        $productOrderDB = new ProductOrderDB();
        $orders = $productOrderDB->requestOrder($payment, $mobile);
        return \response()
            ->json([
                "message" => __("messages.product_orders_list"),
                "data" => $orders->paginate($limit, $page)
            ], 200);
    }

    /**
     * @param Request $request
     */
    public function cancel(Request $request)
    {
        $id = $request->input("id");
        dd($request->product_order_id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            \DB::beginTransaction();
            $userDB = new UserDB();
            $orderController = new OrderController();
            $walletDB = new WalletDB();
            $merchantDB = new MerchantDB();
            $accountDB = new AccountDB();
            $productOrderDB = new ProductOrderDB();
            $costDB = new CostDB();

            /*-------------operator submit order-------------*/
            //get login user instance
            $loginUser = \Auth::user();
            $merchantInstance = $merchantDB->find($loginUser->merchant_id);
            if (!$merchantInstance) {
                return response()
                    ->json([
                        "message" => __("messages.400"),
                        "error" => __("messages.merchant_not_exist")
                    ], 400);
            }

            //get customer instance
            $customerInstance = $userDB->find($request->customer_id);
            if (!($customerInstance instanceof User)) {
                return response()
                    ->json([
                        "message" => __("messages.400"),
                        "error" => __("messages.customer_not_exist")
                    ], 400);
            }

            //create product order
            $productOrderInstance = $productOrderDB->create([
                "merchant_id" => $merchantInstance->id,
                "user_id" => $customerInstance->id,
                "author_id" => $request->author_id,
                "merchant_user_id" => \Auth::id(),
                "description" => isset($request->description) ? $request->description : null,
            ]);

            // get wallet type
            $walletsInstance = $walletDB->getAllInstance();

            $sync = [];
            foreach ($request["costs"] as $cost) {

                foreach ($walletsInstance as $walletInstance) {
                    foreach ($cost as $key => $item) {
                        $sync[$key] = ["quantity" => $item];
                    }
                }
            }
//                $customerInstance
//                $merchantInstance
//                $request->costs


            //sync product order with costs
            $productOrderInstance->costs()->sync($sync);

            //get treasury account ids
            $wallet_id = $walletDB->wallet_ids("rials");
            $treasuryAccountInstance = $accountDB->getTreasuryAccount($wallet_id);

            //get user (account) with cost type
            $fromUserRialAccount = $accountDB->getUserAccount($customerInstance->id, $treasuryAccountInstance->id);
            $merchantRialAccount = $accountDB->getMerchantAccount($merchantInstance->id, $treasuryAccountInstance->id);

            //get treasury account ids
            $wallet_id = $walletDB->wallet_ids("point");
            $treasuryAccountInstance = $accountDB->getTreasuryAccount($wallet_id);

            //get user (account) with cost type
            $fromUserPointAccount = $accountDB->getUserAccount($customerInstance->id, $treasuryAccountInstance->id);
            $merchantPointAccount = $accountDB->getMerchantAccount($merchantInstance->id, $treasuryAccountInstance->id);

            /*----------------------submit wallet order----------------------*/
            //get customer all old product order point amount
            $oldOrderPointAmount = 0;
            foreach ($customerInstance->productOrders as $productOrder) {
                foreach ($productOrder->orders as $order) {
                    if ($order->treasury_account_id == 2) {
                        $oldOrderPointAmount += $order->amount;
                    }
                }
            }

            $responseOrders = [];
            foreach ($request->costs as $wallet => $costs) {

                //sum amount
                $totalAmount = null;
                foreach ($costs as $cost_id => $qty) {
                    //get cost instance
                    $cost = $costDB->getCost($cost_id);
                    $totalAmount += ($cost * $qty);
                }

                //rials calculating
                if ($wallet == "rials") {

                    $wallet_id = $walletDB->wallet_ids($wallet);
                    $rialTreasuryAccountInstance = $accountDB
                        ->getTreasuryAccount($wallet_id);

                    if (!($rialTreasuryAccountInstance instanceof Account)) {
                        return response()
                            ->json([
                                "message" => __("messages.400"),
                                "error" => "your rial treasury account does not exist"
                            ], 400);
                    }

                    /*-------------------- create order for rial ---------------------*/
                    $orderData = [
                        "goods_id" => $productOrderInstance->id,
                        "from_account_id" => $fromUserRialAccount->id,
                        "to_account_id" => $merchantRialAccount->id,
                        "amount" => $totalAmount,
                        "type" => "request",
                        "author_id" => $request->author_id,
                        "paid_at" => null,
                        "treasury_account_id" => $rialTreasuryAccountInstance->id,
                    ];
                    if ($request->has("type")) {
                        $orderInstance = $orderController->store($orderData, $request->type);
                    } else {
                        $orderInstance = $orderController->store($orderData);
                    }
                    $responseOrders["rials"][] = $orderInstance->toArray();
                }

                //point calculating
                if ($wallet == "point") {
                    $credit = $fromUserPointAccount->credits->first()->amount;
                    if ($oldOrderPointAmount + $totalAmount > $credit) {
                        return \response()
                            ->json([
                                "message" => __("messages.400"),
                                "error" => __("messages.not_enough_point_credit"),
                            ], 400);
                    }

                    $wallet_id = $walletDB->wallet_ids($wallet);
                    $pointTreasuryAccountInstance = $accountDB
                        ->getTreasuryAccount($wallet_id);

                    if (!($pointTreasuryAccountInstance instanceof Account)) {
                        return response()
                            ->json([
                                "message" => __("messages.400"),
                                "error" => "your point treasury account does not exist"
                            ], 400);
                    }

                    /*-------------------- create order for point ---------------------*/
                    $orderData = [
                        "goods_id" => $productOrderInstance->id,
                        "from_account_id" => $fromUserPointAccount->id,
                        "to_account_id" => $merchantPointAccount->id,
                        "amount" => $totalAmount,
                        "type" => "request",
                        "author_id" => $request->author_id,
                        "paid_at" => null,
                        "treasury_account_id" => $pointTreasuryAccountInstance->id,
                    ];
                    if ($request->has("type")) {
                        $orderInstance = $orderController->store($orderData, $request->type);
                    } else {
                        $orderInstance = $orderController->store($orderData);
                    }
                    $responseOrders["point"][] = $orderInstance->toArray();
                }
            }
            $productOrderInstance->wallet_orders = $responseOrders;

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return response()
                ->json([
                    "message" => "some thing went wrong",
                    "error" => "your order is not created"
                ], 400);
        }
        return response()
            ->json([
                "message" => "product order submit successfully",
                "data" => $productOrderInstance
            ], 200);
    }

}
