<?php

namespace App\Http\Controllers\Version1;

use App\repo\AccountTypeDB;
use App\Traits\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class AccountTypeController extends Controller
{
    use Response;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $accountTypeDB = new AccountTypeDB();
        $limit = $request->has("limit") ? $request->limit : 10;
        $response = $accountTypeDB->list($limit);
        return $this->successResponse("account_type_list", $response, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            "type" => "required",
            "wallet_id" => "required|numeric",
            "title" => "nullable|max:50",
            "subtitle" => "nullable|max:190",
            "description" => "nullable|max:300",
            "balance_type" => "nullable",
            "min_account_amount" => "nullable",
            "max_account_amount" => "nullable",
            "min_transaction_amount" => "nullable",
            "max_transaction_amount" => "nullable",
            "legal" => "nullable",
            "interest_rate" => "nullable",
            "interest_period" => "nullable",
            "revoked" => "nullable",
        ]);

        if ($validation->fails()) {
            return $this->errorResponse("validation_error", $validation->errors(), \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $accountTypeDB = new AccountTypeDB;

        //create account type
        $accountTypeData = $request->all();
        $accountTypeInstance = $accountTypeDB->create($accountTypeData);

        if ($accountTypeInstance) {
            return $this->successResponse("account_Type_create_success", $accountTypeInstance, 200);
        }
        return $this->errorResponse("400", __("messages.account_type_not_created"), 400);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $accountTypeDB = new AccountTypeDB;
        $accountTypeInstance = $accountTypeDB->find($id);
        return $this->successResponse("account_type", $accountTypeInstance, 200);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        //
    }
}
