<?php

namespace App\Http\Controllers\Version1;

use App\Account;
use App\AccountType;
use App\Credit;
use App\Events\WalletDeletedEvent;
use App\repo\AccountDB;
use App\repo\AccountTypeDB;
use App\repo\CreditDB;
use App\repo\WalletDB;
use App\Traits\Response;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WalletController extends CrudController
{
    use Response;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $walletDB = new WalletDB();
        $response = $walletDB->list();
        return $this->successResponse("wallet_list", $response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|unique:w_wallets|max:100',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse('validation_fail', $validator->errors());
        }

        $walletDB = new WalletDB;
        $data = $request->all();
        $accountDB = new AccountDB();
        $accountTypeDB = new AccountTypeDB();
        $creditDB = new CreditDB();
        try {
            \DB::beginTransaction();

            //create wallet (wallet type)
            $walletInstance = $walletDB->create($data);

            //create account type for treasury
            $accountTypeData = [
                "type" => "treasury",
                "wallet_id" => $walletInstance->id,
                "title" => isset($data["title"]) ? $data["title"] : null,
                "subtitle" => isset($data["subtitle"]) ? $data["subtitle"] : null,
                "description" => isset($data["description"]) ? $data["description"] : null,
                "balance_type" => "zero",
                "min_account_amount" => 0,
                "max_account_amount" => 0,
                "min_transaction_amount" => 0,
                "max_transaction_amount" => 0,
                "legal" => false,
                "interest_rate" => 1,
                "interest_period" => 1,
                "revoked" => false,
            ];
            $instanceTypeAccount = $accountTypeDB->create($accountTypeData);

            //create account for treasury
            $accountData = [
                "account_type_id" => $instanceTypeAccount->id,
                "treasury_account_id" => null,
                "client_type" => null,
                "client_id" => null,
                "revoked" => false,
            ];
            $instanceAccount = $accountDB->create($accountData);

            //treasury account credit
            $creditData = [
                "account_id" => $instanceAccount->id,
                "treasury_account_id" => $instanceAccount->id,
                "amount" => 0,
                "usable_at" => null,
                "expired_at" => null,
                "revoked" => false,
            ];
            $creditDB->create($creditData);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return \response()
                ->json([
                    "message" => "some things went wrong",
                    "error" => "{$e->getMessage()}"
                ], 400);
        }

        //check for create wallet
        if ($walletInstance) {
            return \response()
                ->json([
                    "message" => "wallet successfully created",
                    "data" => $walletInstance
                ], 200);
        }
    }


    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $walletDB = new WalletDB();
        $response = $walletDB->find($id);
        if ($response instanceof Wallet) {
            return $this->successResponse("wallet_info", $response);
        }
        return $this->errorResponse("400", "wallet_not_find");
    }

    /**
     * @param Request $r
     * @param $id
     * @param WalletDB $walletDB
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $r, $id, WalletDB $walletDB)
    {
        $data = $r->all();
        $walletInstance = $walletDB->find($id);
        if (is_null($walletInstance)) {
            return response()
                ->json('id (' . $id . ') dose not exist', 400);
        }
        if ($walletDB->update($id, $data)) {
            return response()->json($walletDB->find($id), 201);
        } else {
            return Response()
                ->json([
                    'message' => "wallet can't be updated, update it again"
                ], 400);
        }
    }

    /**
     * @param $id
     * @param WalletDB $walletDB
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id, WalletDB $walletDB)
    {
        event(new WalletDeletedEvent($id));
        if (is_null($walletDB->find($id))) return response()->json(['id (' . $id . ') dose not exist']);
        try {
            \DB::beginTransaction();
            //delete wallet (wallet type)
            $walletDB->delete($id);


            //after delete wallet delete [accType, acc, credit]
            $accTypeId = AccountType::select("id")->where("wallet_id", $id)->get()->toArray()[0]["id"];
            AccountType::where("wallet_id", $id)->delete();
            Account::where("account_type_id", $accTypeId)->delete();
            credit::where("account_id", $accTypeId)->delete();

            \DB::commit();
            return response()
                ->json('', 204);
        } catch (\Exception $e) {
            \DB::rollBack();
            return \response()
                ->json([
                    "message" => "some things went wrong",
                    "error" => "{$e->getMessage()}"
                ], 400);
        }
    }

    /**
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse
     */
    public function walletCr6663edit(Request $r)
    {
        $idUser = auth()->user()["id"];
        $account = Account::where('user_id', $idUser)
            ->with(['credits', 'accountType'])->whereHas('accountType', function ($q) {
                $q->with("wallet");
            })->get();
        $walletTitle = [];
        $creditInfo = [];
        foreach ($account->toArray() as $key) {
            $credit = $key["credits"][0];
            $walletTitle = Translations::where("id", $key["account_type"]["id"])->get()->toArray()[0]["title"];
            $Arr = ["id" => $credit["id"],
                "club_id" => $credit["club_id"],
                "amount" => $credit["amount"],
                "type" => $walletTitle,
                "expired_at" => $credit["expired_at"],
                "usable_at" => $credit["usable_at"]];

            array_push($creditInfo, $Arr);
        };

        $walletTitle = $account[0]->accountType->wallet->toArray()["title"];
        return Response()->json([
            $creditInfo
        ], 200);
    }
}
