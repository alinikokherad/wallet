<?php

namespace App\Http\Controllers;

use App\Traits\Response;
use App\Traits\Validation;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use  Response, Validation;
    //
}
