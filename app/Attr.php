<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Account
 * @package Mentasystem\Wallet\Entities
 */
class Attr extends Model
{
    protected $table = 'w_attributes';

    protected $fillable = [
        "attribute",
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, "w_value", "attr_id", "entity_id")->withPivot("value");
    }
}
