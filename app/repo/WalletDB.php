<?php
/**
 * Created by PhpStorm.
 * User: a_nikookherad
 * Date: 9/15/19
 * Time: 6:21 PM
 */

namespace App\repo;


use App\Account;
use App\Wallet;

/**
 * Class WalletDB
 * @package App\repo
 */
class WalletDB
{

    /**
     * @param int $limit
     * @return mixed
     */
    public function list($limit = 10)
    {
        $wallets = Wallet::paginate($limit);
        return $wallets;
    }

    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $wallet = Wallet::create($data);
        if ($wallet instanceof Wallet) {
            return $wallet;
        }
        return false;
    }

    /**
     * @param $type
     * @return bool
     */
    public function get($type)
    {
        $instance = Wallet::where("type", $type)->first();
        if ($instance instanceof Wallet) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $instance = Wallet::find($id)->delete();
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id, $data): bool
    {
        return $instance = Wallet::where("id", $id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $instance = Wallet::findorfail($id);
        return $instance;
    }

    /**
     * @param $walletType
     * @return bool
     */
    public function getTreasuryAccountInstanceWithWalletType($walletType)
    {
        $accountInstance = Account::whereHas("accountType", function ($query) {
            $query->where("type", "treasury");
        })->whereHas("accountType.Wallet", function ($query) use ($walletType) {
            $query->where("type", $walletType);
        })->first();
        if ($accountInstance instanceof Account) {
            return $accountInstance;
        }
        return false;
    }

    /**
     * @param $walletType
     * @return bool
     */
    public function getTreasuryAccountInstanceWithWalletId($wallet_id)
    {
        $accountInstance = Account::whereHas("accountType", function ($query) {
            $query->where("type", "treasury");
        })->whereHas("accountType.Wallet", function ($query) use ($wallet_id) {
            $query->where("id", $wallet_id);
        })->first();
        if ($accountInstance instanceof Account) {
            return $accountInstance;
        }
        return false;
    }

    /**
     * get all wallet instances
     * @return mixed
     */
    public function getAllInstance()
    {
        $instances = Wallet::get();
        return $instances;
    }


}
