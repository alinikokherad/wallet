<?php


namespace App\repo;

use App\Status;
use Illuminate\Support\Facades\DB;

/**
 * Class StatusDB
 * @package App\repo
 */
class StatusDB
{

    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $instance = Status::create($data);
        if ($instance instanceof Status) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $limit
     * @return mixed
     */
    public function get($limit)
    {
        $response = Status::paginate($limit);
        return $response;
    }

    /**
     * @param $account_id
     * @return bool
     */
    public function find($account_id)
    {
        $instance = Status::where("id", $account_id)->first();
        if ($instance instanceof Status) {
            return $instance;
        }
        return false;
    }

    /**
     * @return Status[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $instance = Status::all();
        return $instance;
    }

}
