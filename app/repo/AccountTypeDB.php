<?php
/**
 * Created by PhpStorm.
 * User: a_nikookherad
 * Date: 9/21/19
 * Time: 2:57 PM
 */

namespace App\repo;


use App\Account;
use App\AccountType;

class AccountTypeDB
{

    /**
     * @param int $limit
     * @return mixed
     */
    public function list($limit = 10)
    {
        $instance = AccountType::paginate($limit);
        return $instance;
    }

    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $instance = AccountType::create($data);
        if ($instance instanceof AccountType) {
            return $instance;
        }
        return false;
    }

    /**
     * @param $account_id
     * @return mixed
     */
    public function getAccountTypeWithAccountId($account_id)
    {
        $account = Account::where("id", $account_id)->with("accountType")->first();
        return $account->accountType->type;
    }

    /**
     * get all treasury account type
     */
    public function getTreasury()
    {
        $instances = AccountType::where("type", "treasury")->get();
        return $instances;
    }

    /**
     * @param $accountType
     * @return mixed
     */
    public function getAccountTypeWithType($accountType)
    {
        // get account type instance
        $instances = AccountType::where("type", $accountType)->get();
        return $instances;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $instance = AccountType::where("id", $id)->with("accounts")->first();
        return $instance;
    }

    /**
     * @return mixed
     */
    public function getAllTreasuryAccountType()
    {
        return AccountType::where("type", "treasury")->get();
    }

    /**
     * @param $wallet_id
     * @return bool
     */
    public function getAccountTypeWithWalletId($wallet_id)
    {
        $instance = AccountType::where("wallet_id", $wallet_id)->where("type", "=", "treasury")->first();
        if ($instance instanceof AccountType) {
            return $instance;
        }
        return false;
    }
}
