<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Account
 * @package Mentasystem\Wallet\Entities
 */
class Account extends Model
{
    protected $table = 'w_accounts';

    protected $fillable = [
        "user_id",
        "treasury_account_id",
        "client_type",
        "client_id",
        "account_type_id",
        "revoked",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function credits()
    {
        return $this->hasMany(Credit::class, "account_id");
    }

    public function accountType()
    {
        return $this->belongsTo(AccountType::class);
    }

    /*    public function account()
        {
            return $this->hasOne(Account::class, "treasury_account_id", "id");
        }*/

    public function clubs()
    {
        return $this->belongsToMany(\Club::class, "club_account", "account_id", "club_id")->withPivot(["account_type", "club_type"]);
    }

    public function fromAccountOrder()
    {
        return $this->hasMany(Order::class, "from_account_id");
    }

    public function toAccountOrder()
    {
        return $this->hasMany(Order::class, "to_account_id");
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }
}
