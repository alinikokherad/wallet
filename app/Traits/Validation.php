<?php


namespace App\Traits;


use Illuminate\Support\Facades\Validator;

trait Validation
{

    public function validation($request, $rule)
    {
        $validation = Validator::make($request, $rule);
        if (!$validation->fails()) {
            return false;
        }
        return $this->errorResponse("validation_error", $validation->errors(), \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
