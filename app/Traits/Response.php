<?php


namespace App\Traits;


/**
 * api json Response
 * @package App\Treates
 */
trait Response
{

    /**
     * @param $message
     * @param $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse($message, $data, $code = 200)
    {
        return response()
            ->json([
                "message" => __("messages.{$message}"),
                "data" => $data
            ], $code);
    }

    /**
     * @param $message
     * @param $error
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse($message, $error, $code = 400)
    {
        return response()
            ->json([
                "message" => __("messages.{$message}"),
                "error" => $error
            ], $code);
    }
}
