<?php

namespace App\Events;

use App\Basket;
use Illuminate\Queue\SerializesModels;

class CreatedOrderEvent
{
    use SerializesModels;
    public $order;
    public $request;
    public $fromAccountInstance;
    public $toAccountInstance;
    public $treasuryInstance;

    /**
     * CreatedOrderEvent constructor.
     * @param Basket $order
     * @param $request
     * @param $fromAccountInstance
     * @param $toAccountInstance
     * @param $treasuryInstance
     * @param $productOrderInstance
     */
    public function __construct(Basket $order, $request = null, $fromAccountInstance = null, $toAccountInstance = null, $treasuryInstance = null)
    {
        $this->order = $order;
        $this->request = $request;
        $this->fromAccountInstance = $fromAccountInstance;
        $this->toAccountInstance = $toAccountInstance;
        $this->treasuryInstance = $treasuryInstance;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
