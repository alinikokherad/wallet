<?php

namespace App\Events;

use App\Account;
use Illuminate\Queue\SerializesModels;

class AccountCreatedEvent
{
    use SerializesModels;
    public $account;
    public $data;

    /**
     * AccountCreatedEvent constructor.
     * @param Account $account
     * @param $data
     */
    public function __construct(Account $account, $data)
    {
        $this->account = $account;
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
